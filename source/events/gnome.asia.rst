GNOME.Asia
==========

GNOME.Asia Summit is an annual conference for GNOME users and developers in Asia. The event focuses primarily on the GNOME desktop and other devices that use GNOME, and also covers GNOME-based applications and GNOME development platform tools. It brings together the GNOME community in Asia to provide a forum for users, developers, foundation leaders, governments and businesses to discuss both the present technologies and future developments.

Organizing GNOME.Asia
---------------------

Every GNOME.Asia Summit relies on local volunteers to help organize and run the conference. If you are interested in hosting a GNOME.Asia conference, you can contact ``info@gnome.org`` or ``asia@gnome.org`` for more information. You can also look out for the call for bids that are posted each year on the `GNOME Foundation blog <https://foundation.gnome.org/news/>`_. 

Previous GNOME.Asia Summits
---------------------------

.. list-table::
  :widths: 20 80
  :header-rows: 1

  * - Year
    - Location
  * - `2023 <https://events.gnome.org/event/170/>`_
    - Kathmandu (Nepal)
  * - `2022 <https://events.gnome.org/event/100/>`_
    - Kuala Lumpur
  * - `2021 <https://events.gnome.org/event/94/>`_
    - Online only
  * - 2020
    - Online only
  * - 2019
    - Gresik (Indonesia)
  * - 2018
    - Taipei (Taiwan)
  * - 2017
    - Chongqing (China)
  * - 2016
    - Delhi (India)
  * - 2015
    - Depok (Indonesia)
  * - 2014
    - Beijing (China)
  * - 2013
    - Seoul (Korea)
  * - 2012
    - Hong Kong
  * - 2011
    - Bangalore (India)
  * - `2010 <https://web.archive.org/web/20131018041741/http://2010.gnome.asia/>`_
    - Taipei (Taiwan)
  * - 2009
    - Ho-Chi-Minh (Vietnam)
  * - `2008 <https://web.archive.org/web/20130901220316/http://2008.gnome.asia/en/>`_
    - Beijing (China)
