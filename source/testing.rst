Testing
=======

This page provides information on how to run unstable development versions of GNOME. Doing this regularly and :doc:`reporting issues </issues/reporting>` is a valuable way to contribute to GNOME.

Nightly Flatpaks
----------------

GNOME publishes nightly builds of its apps as Flatpaks. This is a great way to safely run the very latest development versions of GNOME apps.

See `nightly.gnome.org <https://nightly.gnome.org/>`_ for details.

GNOME OS
--------

GNOME OS is a complete operating system that can be run in a virtual machine, which includes recent development versions of the GNOME desktop and apps. Once installed, you can update it to updated to the latest nightly GNOME versions.

See `os.gnome.org <https://os.gnome.org/>`_ for details.

Distributions
-------------

GNOME development versions are also available to use through some Linux distributions.

Build the code
--------------

If the above solutions don't meet your needs, you may need to :doc:`build the software yourself </development/building>`.
