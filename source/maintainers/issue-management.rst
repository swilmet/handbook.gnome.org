Issue Management
================

This page provides guidelines and advice for those who are responsible for GNOME project issue trackers (primarily maintainers).

Feature request policies
------------------------

Feature requests submitted through issue trackers can be difficult to resolve and can become adversarial. It can therefore be beneficial for modules to not accept feature requests through their issue trackers, and to have alternative arrangements for new feature suggestions. If a module's maintainers decide to adopt this policy:

* it should be clearly documented (in the project's issue template and/or README)
* an alternative forum should be provided for feature discussions, such as a Discourse tag and/or Matrix chat room
* the location of the alternative forum should be advertised in the project description in Gitlab as well as in the project new issue template
* maintainers should ideally engage with feature suggestions and not simply ignore them; however, they shouldn't feel obliged to approve or reject each and every proposal

Issues can still be used to track new features when a no feature requests policy is in place. However, this should only be done after a new feature has been discussed and there is a consensus on what is wanted.

Set a good example
------------------

If established contributors don't follow our issue guidelines, then we cannot expect external participants to do the same. It is therefore important to lead by example:

* File issues for bugs before opening an MR that contains the fix.
* When filing issues yourself, fill out the issue template and follow the :doc:`issue reporting guidelines </issues/reporting>`.
* Discuss feature additions and changes using an appropriate forum (Matrix/Discourse/GitLab) and get rough consensus before opening an MR.
* In general, avoid having open-ended discussions in the issue tracker, such as discussing whether a feature is needed. If there are occasions when you do want to use an issue to have a discussion, clearly flag it as such using the **RFC** label. It is recommended to close such issues after two weeks of inactivity.

Encourage contributors
----------------------

Having contributors do issue reviews makes life much easier for maintainers, and maintainers are strongly encouraged to help themselves by encouraging contributions via issue reviews:

* Add issue review to your project's contribution guide, with a link to the :doc:`review guide </issues/review>`.
* Be on the lookout for potential issue review contributors. If someone is filing issues, it's a good opportunity to ask if they'd be interested in helping out with reviews.
* Giving out additional issue tracker permissions is a great way to motivate a new contributor, so don't be afraid to do it. You can always tell the contributor what they should and shouldn't do, and if you are watching the issue tracker then it's easy to intervene if necessary.
   * Additional issue tracker permissions can be granted by assigning the Reporter role in GitLab. Be aware that accounts with this role can read and write to confidential issues.
* Once a new contributor has gained some :ref:`experience with basic tasks <contributing issue review>`, encourage them to take on more responsibility.

Encouraging contributors to help with the issue tracker can sometimes feel scary, but it doesn't need to be. The risks are relatively low, and the rewards can be huge.
