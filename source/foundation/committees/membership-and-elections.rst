Membership & Elections Committee
================================

Welcome to the Membership & Elections Committee of the GNOME Foundation.

The Membership & Elections Committee, as the name suggests, is tasked with processing applications for membership to the GNOME Foundation and based on a set of policies approve, reject or ask for more information on the applications. The Membership & Elections Committee are also responsible for conducting the Elections to the GNOME Foundation's Board of Directors.

Current members of the committee are:

* Andrea Veri (chair)

Useful links
------------

* `Application and renewal form <https://gitlab.gnome.org/Teams/MembershipCommittee/issues/new?issuable_template=membership-application>`_
* `Membership discussions on Discourse <https://discourse.gnome.org/c/community/membership/338>`_
* `GNOME Foundation Elections and Referenda <https://vote.gnome.org>`_
