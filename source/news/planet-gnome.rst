Planet GNOME
============

`Planet GNOME <https://planet.gnome.org>`_ is an aggregator of blogs with content relevant to the GNOME community. 

To be on Planet GNOME a blog needs to be maintained by a GNOME Foundation member or a Google Summer of Code/Outreachy intern.

Additions, updates, and fixes to Planet GNOME should be reported as `issues against the planet-web project <https://gitlab.gnome.org/Infrastructure/planet-web/-/issues>`_.

Getting your blog ready for Planet GNOME
----------------------------------------

Steps to have your blog added to Planet GNOME:

* Check the `Feed Validator <https://www.feedvalidator.org/>`_ for irregularities.
* Make sure your content complies with the Planet GNOME rules.
* File a `GitLab issue <https://gitlab.gnome.org/Infrastructure/planet-web/-/issues>`_ requesting your blog to be added.

Planet GNOME rules
------------------

* All content should respect the `GNOME Code of Conduct <https://conduct.gnome.org/>`_.
* All the posts syndicated should be in English (blog platforms such as Wordpress allow for setting a category with feeds for your English posts).
* Your RSS/ATOM feed should be configured to show the full post (disable the "read the full story" link).

What content is good for Planet GNOME?
--------------------------------------

While blog content should be useful and interesting to the GNOME community, members are free to share posts that aren't directly related to GNOME. Our community is made of people and we love reading your latest food recipe or personal project.

Make sure your post cadence doesn't flood Planet GNOME with your posts. If you post too frequently, consider making a category with a different RSS feed for Planet GNOME.

Contact
-------

Planet GNOME is currently maintained by `Felipe Borges <https://gitlab.gnome.org/felipeborges>`_.
