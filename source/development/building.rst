Building
========

GNOME modules vary in nature, and include everything from system libraries, through to apps and system services. As a result, they vary in terms of which tools are needed to build them, and the relative complexity involved in building each one.

This page provides an overview of the available build tools and methods. It assumes that you are building GNOME using a recent version of a Linux distribution.

Whenever you want to build a module, it is recommended to check their ``README`` files for any module-specific build instructions.

Meson
-----

`Meson <https://mesonbuild.com/>`_ is the standard build system used by GNOME projects. In many cases it is used indirectly through another tool, such as those described below. However, there are occasions when it might be used directly using the ``meson`` and ``ninja`` commands, as specified in per-module build guides.

Builder
-------

`Builder <https://apps.gnome.org/en-GB/Builder/>`_ is GNOME's IDE. It allows building apps with the click of a button. It automatically takes care of any dependencies that are required by the app, and it builds each project in its own isolated environment, so that it won't interfere with your host system. This makes it an excellent choice for building most GNOME apps.

`welcome.gnome.org <https://welcome.gnome.org>`_ includes tutorials for building different GNOME apps this way. You can also read `Builder's documentation <https://builder.readthedocs.io/>`_.

Toolbx
------

`Toolbx <https://containertoolbx.org/>`_ is a tool for creating nicely integrated containers that can be used as development environments without interfering with your host operating system. Toolbx is an appropriate choice when building:

* libraries
* system services
* apps that need resources which cannot be accessed from a Flatpak sandbox (such as hardware or certain APIs)
* modules that need access to development versions of system services, for example Settings and Initial Setup

See :doc:`building with Toolbx </development/toolbx>` for more information on how to build GNOME modules using Toolbox as a build environment.

JHBuild (legacy)
----------------

`JHBuild <https://gnome.pages.gitlab.gnome.org/jhbuild/>`_ is a command line tool which allows building GNOME modules along with any required dependencies. JHBuild also provides an environment in which modules and services can be built and run without interfering with the rest of your system.

JHBuild can be used in the same circumstances as Toolbx.
