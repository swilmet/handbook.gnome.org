Stack Traces
============

When a program on your computer crashes, a core dump is generated which stores
the state of the program at the time of the crash. The core dump can be used to
generate a stack trace, which allows developers to investigate and hopefully fix
the crash. Developers expect you to attach a stack trace to your issue report
whenever reporting a crash. This page describes how to generate this stack
trace.

Stack traces are also called "backtraces." They are the same thing.

Prerequisites
-------------

This guide uses a simple method to generate stack traces, which works on many
modern Linux systems. To check whether you can use this simple method, you need
to verify whether your system has both systemd-coredump and debuginfod enabled.

To check whether systemd-coredump is enabled, run::

  $ cat /proc/sys/kernel/core_pattern

This should return some text that includes ``systemd-coredump``.

To check whether debuginfod is enabled, run::

  $ echo $DEBUGINFOD_URLS

This should return a URL.

If either of these tests fail, then you will need to get the stack trace
manually. See `this blog post <https://blogs.gnome.org/mcatanzaro/2021/09/18/creating-quality-backtraces-for-crash-reports/>`_
for details on how to do this.

Simple stack traces
-------------------

If your system has the prerequisites described above, and the crash you are
reporting is not from a Flatpak app, you can follow the steps below.

First install ``gdb``, the GNU debugger, if it's not already installed. Then
simply run these commands::

  $ coredumpctl gdb
  (gdb) bt full

This will generate a stack trace for the last crash that happened on the system.
As this happens you will likely be asked some questions:

* If you are asked ``Enable debuginfod for this session? (y or [n])``, enter
  ``y`` and press return.
* If you are asked whether to continue without paging, enter ``c`` and press
  return. (You may need to do this multiple times.)
* Once the prompt shows ``(gdb)`` again, then the full stack trace has been
  printed. Enter ``q`` and press return to quit.

You can now copy the output to a text file and attach it to your issue report.
That's it! You're done.

Stack traces for Flatpak apps
-----------------------------

Getting a stack trace for a Flatpak app is very similar to the method
described above, but requires some extra setup steps.

Install debug extensions
~~~~~~~~~~~~~~~~~~~~~~~~

To get a stack trace for most Flatpak apps, including those distributed
by GNOME or by Flathub, you have to first install debug extensions for both the
Flatpak app and the runtime that it uses.

To start, get the ID of the app that you want to generate the stack trace for.
To do this, run ``flatpak list`` and check the Application ID column for the
app. For example, for GNOME Web the app ID appears as ``org.gnome.Epiphany``. 

Next, confirm the ID of the runtime that the app uses. For example::

  $ flatpak info org.gnome.Epiphany | grep Runtime
  Runtime: org.gnome.Platform/x86_64/master

In this example, ``org.gnome.Platform`` is the ID of the runtime. This is the
GNOME runtime.

Now it's time to install the debug extensions. Here, you need to install the
app's debug extension, the SDK which matches the runtime being used by the app,
and the SDK's debug extension. For example, for GNOME Web using the GNOME
runtime, you would run::

  $ flatpak install org.gnome.Epiphany.Debug
  $ flatpak install org.gnome.Sdk
  $ flatpak install org.gnome.Sdk.Debug

Note: if the crash happened in a Flatpak built by Fedora, there are no debug
extensions. Instead, you will only need to install the
``org.fedoraproject.Sdk`` runtime corresponding to the version of the
``org.fedoraproject.Platform`` that the application uses.

Finally, you will need to run ``flatpak update`` to be certain that the
versions of the debug extensions exactly match the version of the runtime and
application::

  $ flatpak update

Get the stack trace
~~~~~~~~~~~~~~~~~~~

Once you have installed the debug extensions, you're ready to take the stack
trace. This works similarly to taking a stack trace for processes that aren't
Flatpak apps, but you'll use the ``flatpak-coredumpctl`` command instead of
using ``coredumpctl`` directly, like so::

  $ flatpak-coredumpctl org.gnome.Epiphany
  (gdb) bt full

If neither the app nor the runtime was updated when you ran ``flatpak update``,
then you can run this without having to wait for the app to crash again.
However, if either the app or runtime was updated, you'll need to reproduce the
crash before you can run the commands.

Then follow the rest of the simple stack trace instructions to view the stack
trace and copy it.
