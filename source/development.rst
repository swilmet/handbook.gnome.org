Development
===========

This section of the GNOME handbook provides general information about how to do development work in GNOME. This includes how to propose a change to a GNOME module, how to write commit messages, how to license your code, and how to build projects.

It is not intended as a technical guide to the GNOME platform or as an introduction to GNOME development. For these things, please see `welcome.gnome.org <https://welcome.gnome.org>`_ and `developer.gnome.org <https://developer.gnome.org/>`_.

.. toctree::
   :hidden:

   development/change-submission
   development/commit-messages
   development/building
   development/toolbx
   development/legal
